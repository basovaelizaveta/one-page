$(function(){
	var regEmail = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	var regPhone = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;

    $('html').css({'height': $(window).height() + 'px', 'overflow' :'hidden'});
    $('.block-request').addClass('fjs-position');

	$('.fjs--test-email').change(function() {
		if (!regEmail.test($(this).val())) {
			$(this).parent().find('.fjs--test-alert').remove();
			$(this).parent().append('<span class="fjs--test-alert">Введите корректный email адрес</span>');
		} else {
			$(this).parent().find('.fjs--test-alert').remove();
		}
	});
	$('.fjs--test-phone').change(function() {
		if (!regPhone.test($(this).val())) {
			$(this).parent().find('.fjs--test-alert').remove();
			$(this).parent().append('<span class="fjs--test-alert">Введите корректный телефонный номер</span>');
		}else {
			$(this).parent().find('.fjs--test-alert').remove();
		}
	});

	$('form').submit(function(e){
        var that = $(this);
        $.ajax({
            url:        that.attr('action'),
            type:       "post",
            dataType:   "html",
            data:       that.serialize(),
            success: function(response){
                if(response == 'success') {
                    that.children().each(function(i,e){
                        $(e).animate({'opacity': 0}, 300*(i+1),function(){
                            if($(this).attr('type') == 'submit'){
                                $(this).parent().animate({'height':0},function(){
                                    $(this).addClass('hide');
                                });
                                $('.thank h1').fadeOut(0).parent().find('p').fadeOut(0).parent().
                                removeClass('hide').find('h1').fadeIn(500,function(){
                                    $(this).parent().find('p').fadeIn(500);
                                });
                            }
                        });
                    });
                    that.parent('.block-request__form').find('h3').text('Ваша заявка отправлена, спасибо!');
                } else {
                    that.find('[name = '+response+']').removeClass('success').addClass('error');
                }
            }
        });
	return false;
    });
})