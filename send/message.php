<?php
$name = $_POST['name'];
$surname = $_POST['surname'];
$phone = $_POST['phone'];
$email = $_POST['email'];
$link = $_POST['link'];

if (!$name) {
	die('name');
}

if (!preg_match('/^[a-zA-Zа-яА-Я ]+$/u', $name)) {
	die('name');
}

if (!$phone) {
	die('phone');
}

if (!preg_match("/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/", $email)) {
	die('email');
}

if (!preg_match('/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', $phone)) {
	die('phone');
}


require_once 'lib/Swift-5.0.1/lib/swift_required.php';

$transport = Swift_MailTransport::newInstance();

// Create the Mailer using your created Transport
$mailer = Swift_Mailer::newInstance($transport);


$body  = 'Заявка с сайта на помощь в оформление входных групп в соответствии с дизайн-регламентом.'. "\n";
$body .= 'Имя: ' . $name . ' ' . $surname . "\n";
$body .= 'Телефон: ' . $phone . "\n";
$body .= 'Email: ' . (!$_POST['email'] || $_POST['email'] == 'Email'?'Не указан':$_POST['email']) . "\n";
$body .= 'Ссылка: ' . $link . "\n";

$subject = 'Заявка с сайта.';


//Create a message
$message = Swift_Message::newInstance($subject)
  ->setFrom(array('not_replay@easyconf.in' => 'EasyConf - Робот'))
  ->setTo(array('say@vkamode.ru'))
  ->setBody($body);

// Send the message
$result = $mailer->send($message);

echo 'success';
